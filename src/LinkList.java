
public class LinkList {
    private Link first;

    public LinkList() {
        first = null;
    }

    public Link getFirst() {
        return first;
    }

    public boolean isEmpty() {
        return (first == null);
    }

    public void insertFirst(int id, double dd) {
        Link newLink = new Link(id, dd);
        newLink.next = first;
        first = newLink; // first คือตัวแรก จากการเพิ่ม newLink
    }

    public Link deleteFirst() {
        Link temp = first;
        first = first.next;
        return temp;
    }

    public void displayList() {

        System.out.println("List (first => last): ");
        Link current = first; // current เพื่อเป็น ตัวชี้
        while (current != null) {
            current.displayLink();
            current = current.next;
        }
        System.out.println(" ");
    }

    public void displayLastList(Link current) {
        while (current != null) {
            current = current.next;
            displayLastList(current);
            current.displayLink();
        }
    }
}